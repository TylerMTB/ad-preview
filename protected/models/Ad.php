<?php

class Ad extends CActiveRecord
{
	public function tableName()
	{
		return 'ad';
	}
	
	public function rules()
	{
		return array(
			array('image, headline, description, text, learn_more, username', 'default'),
		);
	}
	
	public function relations()
	{
		return array(
		);
	}
	
	public function attributeLabels()
	{
		return array(
		);
	}
	
	/**
		* Returns the static model of the specified AR class.
		* Please note that you should have this exact method in all your CActiveRecord descendants!
		* @param string $className active record class name.
		* @return Post the static model class
		*/
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
?>