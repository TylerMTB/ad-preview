<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionPreview()
	{
		if($_POST['learn_more'] == "on")
			$_POST['learn_more'] = 1;
		
		$this->renderPartial("preview", array("data" => $_POST));
	}
	
	public function actionSaveAd()
	{
		$ad = new Ad();
		
		if($_POST['learn_more'] == "on")
			$_POST['learn_more'] = 1;
		
		$ad->attributes = $_POST;
		$ad->created = date("Y-m-d H:i:s");
		
		if($ad->save())
		{
			$this->redirect($this->createUrl("site/viewAd", array("id" => $ad->ad_id)));
		}
	}
	
	public function actionViewAd($id)
	{
		$this->render('view', array("data" => (array)Ad::model()->findByPk($id)->attributes));
	}
	
	public function actionUserAds($username)
	{
		$criteria = new CDbCriteria();
		
		$criteria->compare("username", $username);
		
		$ads = new CActiveDataProvider(Ad::model(), array(
			'criteria' => $criteria,
			'sort'=>array(
				'defaultOrder'=>'ad_id DESC',
			)
		));
		
		$this->render('userAds', array("ads" => $ads, "username" => $username));
	}
}