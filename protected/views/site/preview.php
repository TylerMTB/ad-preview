<!-- Nav tabs -->
<ul id="preview_tab" class="nav nav-tabs" role="tablist">
	<li<?php if($data['tab'] == 'desktop' || !$data['tab']):?> class="active"<?php endif; ?> data-tab="desktop"><a href="#desktop" role="tab" data-toggle="tab">Desktop news feed</a></li>
	<li<?php if($data['tab'] == 'mobile'):?> class="active"<?php endif; ?> data-tab="mobile"><a href="#mobile" role="tab" data-toggle="tab">Mobile news feed</a></li>
	<li<?php if($data['tab'] == "right"):?> class="active"<?php endif; ?> data-tab="right"><a href="#right" role="tab" data-toggle="tab">Right column</a></li>
	<?php if($createNew == true): ?>
	<li><a href="/" role="tab">Create a new ad</a></li>
	<li><a href="<?php echo Yii::app()->createUrl("site/userAds", array("username" => $data['username'])); ?>" role="tab">View more from <?php echo CHtml::encode($data['username']); ?></a></li>
	<?php endif; ?>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div class="tab-pane<?php if($data['tab'] == 'desktop' || !$data['tab']):?> active<?php endif; ?>" id="desktop">
		<div class="panel panel-desktop" style="width: 496px">
			<div class="panel-body" style="border-top: none">

				<div class="clearfix bar">
					<div class="pull-left avatar">
						<img width="40" height="40" src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/t1.0-1/p50x50/10301513_628176427262332_1242417506983974766_n.png">
					</div>
					<div class="pull-left">
						<div><a href="javascript:;" class="page-name">Name of the page</a></div>
						<div class="time">Sponsored (demo)</div>
					</div>
				</div>

				<div class="text"><?php echo CHtml::encode($data['text']); ?></div>

				<img class="image img-responsive" src="<?php echo $data['image']; ?>">

				<div class="inner">
					<div class="headline"><?php echo CHtml::encode($data['headline']); ?></div>

					<div class="description"><?php echo CHtml::encode($data['description']); ?></div>

					<?php if($data['learn_more'] == 1): ?>
					<button class="btn btn-learn-more">Learn More</button>
					<div class="url">published june 2014</div>
					<?php else: ?>
					<div class="url">www.domain.com</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane<?php if($data['tab'] == 'mobile'):?> active<?php endif; ?>" id="mobile">
		<div class="panel panel-mobile" style="width: 306px">
			<div class="panel-body" style="border-top: none">

				<div class="clearfix bar">
					<div class="pull-left avatar">
						<img width="40" height="40" src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfp1/t1.0-1/p50x50/10301513_628176427262332_1242417506983974766_n.png">
					</div>
					<div class="pull-left">
						<div><a href="javascript:;" class="page-name">Name of the page</a></div>
						<div class="time">Sponsored (demo)</div>
					</div>
				</div>

				<div class="text"><?php echo CHtml::encode($data['text']); ?></div>

				<img class="image img-responsive" src="<?php echo $data['image']; ?>">

				<div class="inner">
					
					<div class="header">
						<div class="headline"><?php echo CHtml::encode($data['headline']); ?></div>
						<div class="description">
							<?php echo CHtml::encode($data['description']); ?>
						</div>
					</div>
					
					<?php if($data['learn_more'] == 1): ?>
					<button class="btn btn-learn-more">Learn More</button>
					<div class="url">published june 2014</div>
					<?php else: ?>
					<div class="url">www.domain.com</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane<?php if($data['tab'] == 'right'):?> active<?php endif; ?>" id="right">
		<div class="panel panel-right" style="width: 266px">
			<div class="panel-body" style="border-top: none">

				<div class="clearfix">
					<div><a href="javascript:;" class="page-name"><?php echo CHtml::encode($data['headline']); ?></a></div>
					<div class="url">www.domain.com</div>
				</div>
				
				<div class="clearfix">
					<div class="pull-left" style="width: 100px; margin-right: 8px">
						<img class="image img-responsive" src="<?php echo $data['image']; ?>">
					</div>
					<div class="text"><?php echo CHtml::encode($data['text']); ?></div>
				</div>
			</div>
		</div>
	</div>
</div>