<div class="panel panel-default">
	<div class="panel-heading clearfix">
		<h1 class="panel-title pull-left">Viewing ads for: <?php echo CHtml::encode($username); ?></h1>
		<a class="pull-right" href="/"><button class="btn btn-primary btn-xs">Create new ad</button></a>
	</div>
	
	<div class="panel-body">
	
	</div>
	
	<?php
	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider' => $ads,
		'cssFile' => false,
		'template' => '{items} {pager}',
		'itemsCssClass' => 'table',
		'columns' => array(
			'headline',
			'text',
			'description',
			'username',
			array(
				'class' => 'CLinkColumn',
				'header' => '',
				'labelExpression' => '"<button class=\"btn btn-primary btn-xs\">Go to ad</button>"',
				'urlExpression' => 'Yii::app()->createUrl("site/viewAd", array("id" => $data->ad_id))',
				'linkHtmlOptions' => array("target" => "_blank"),
			),
		),
	));
	?>
	
</div>